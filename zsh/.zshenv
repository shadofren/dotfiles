GOPATH=/home/yuu/dev/golang
GOROOT=/usr/lib/go
PATH=$PATH:~/.local/bin/
PATH=$PATH:$GOPATH/bin:$GOROOT/bin
NPMPATH=/home/yuu/.npm-global
PATH=$PATH:$NPMPATH/bin
PATH=$PATH:~/.local/share/nvim/mason/bin
PATH=$PATH:~/Android/Sdk/emulator
