HISTSIZE=10000
SAVEHIST=$HISTSIZE
HISTFILE=~/.zsh/.histfile
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups


setopt autocd
if [[ -t 0 && $- = *i* ]]
then
    stty -ixon
fi

source $HOME/.shortcuts # Load shortcut aliases
source $HOME/.aliases

vf() { $EDITOR $(fzf) ;}

# Zsh theme
fpath=(
    ~/.zsh/pure
    /usr/share/zsh/site-functions
    $fpath
)
autoload -U promptinit; promptinit
zstyle :prompt:pure:path color '#88c0d0'
zstyle ':prompt:pure:prompt:*' color '#5e81ac'
zstyle :prompt:pure:git:branch color '#b48ead'
zstyle :prompt:pure:git:dirty color '#d08770'
prompt pure

# Plugins
autoload -U /usr/share/zsh/site-functions/_golang
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-you-should-use/you-should-use.plugin.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh 2>/dev/null

# Word style
# the non-alphanumeric that is considered part of a word
# the following is missing / in order to delete word on directory
WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'

# Zsh keybindings
bindkey "^[[1;5D" backward-word
bindkey "^B" backward-word
bindkey "^[[1;5C" forward-word
bindkey "^F" forward-word
bindkey "^W" backward-kill-word
bindkey '^A' beginning-of-line
bindkey '^E' end-of-line
bindkey '^K' kill-line
bindkey '^U' backward-kill-line
bindkey '^R' history-incremental-search-backward
bindkey '^p' history-search-backward
bindkey '^n' history-search-forward

# Enable colors and change prompt:
autoload -U colors && colors

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"

# fzf integration
eval "$(fzf --zsh)"

# zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

fastfetch

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/yuu/dev/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/yuu/dev/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/yuu/dev/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/yuu/dev/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

eval "$(zoxide init --cmd cd zsh)"
