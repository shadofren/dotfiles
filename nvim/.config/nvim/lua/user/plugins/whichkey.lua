return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  dependencies = {
    { "echasnovski/mini.nvim", version = false },
  },
  opts = {
    ---@type false | "classic" | "modern" | "helix"
    preset = "classic",
    -- Delay before showing the popup. Can be a number or a function that returns a number.
    ---@type number | fun(ctx: { keys: string, mode: string, plugin?: string }):number
    delay = function(ctx)
      return ctx.plugin and 0 or 200
    end,
    filter = function(mapping)
      -- example to exclude mappings without a description
      -- return mapping.desc and mapping.desc ~= ""
      return true
    end,
    --- You can add any mappings here, or use `require('which-key').add()` later
    spec = {},
    -- show a warning when issues were detected with your mappings
    notify = true,
    plugins = {
      marks = true,  -- shows a list of your marks on ' and `
      registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
      -- the presets plugin, adds help for a bunch of default keybindings in Neovim
      -- No actual key bindings are created
      spelling = {
        enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
        suggestions = 20, -- how many suggestions should be shown in the list?
      },
      presets = {
        operators = true, -- adds help for operators like d, y, ...
        motions = true,  -- adds help for motions
        text_objects = true, -- help for text objects triggered after entering an operator
        windows = true,  -- default bindings on <c-w>
        nav = true,      -- misc bindings to work with windows
        z = true,        -- bindings for folds, spelling and others prefixed with z
        g = true,        -- bindings for prefixed with g
      },
    },
    win = {
      -- don't allow the popup to overlap with the cursor
      no_overlap = true,
      -- width = 1,
      -- height = { min = 4, max = 25 },
      -- col = 0,
      -- row = math.huge,
      -- border = "none",
      padding = { 1, 2 }, -- extra window padding [top/bottom, right/left]
      title = true,
      title_pos = "center",
      zindex = 1000,
      -- Additional vim.wo and vim.bo options
      bo = {},
      wo = {
        -- winblend = 10, -- value between 0-100 0 for fully opaque and 100 for fully transparent
      },
    },
    layout = {
      width = { min = 20 }, -- min and max width of the columns
      spacing = 3,       -- spacing between columns
      align = "left",    -- align columns left, center or right
    },
    keys = {
      scroll_down = "<c-d>", -- binding to scroll down inside the popup
      scroll_up = "<c-u>", -- binding to scroll up inside the popup
    },
    ---@type (string|wk.Sorter)[]
    --- Add "manual" as the first element to use the order the mappings were registered
    --- Other sorters: "desc"
    sort = { "local", "order", "group", "alphanum", "mod", "lower", "icase" },
    ---@type number|fun(node: wk.Node):boolean?
    expand = 1, -- expand groups when <= n mappings
    -- expand = function(node)
    --   return not node.desc -- expand all nodes without a description
    -- end,
    ---@type table<string, ({[1]:string, [2]:string}|fun(str:string):string)[]>
    replace = {
      key = {
        function(key)
          return require("which-key.view").format(key)
        end,
        -- { "<Space>", "SPC" },
      },
      desc = {
        { "<Plug>%((.*)%)", "%1" },
        { "^%+",            "" },
        { "<[cC]md>",       "" },
        { "<[cC][rR]>",     "" },
        { "<[sS]ilent>",    "" },
        { "^lua%s+",        "" },
        { "^call%s+",       "" },
        { "^:%s*",          "" },
      },
    },
    icons = {
      breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
      separator = "➜", -- symbol used between a key and it's label
      group = "+", -- symbol prepended to a group
      ellipsis = "…",
      --- See `lua/which-key/icons.lua` for more details
      --- Set to `false` to disable keymap icons
      ---@type wk.IconRule[]|false
      rules = {},
      -- use the highlights from mini.icons
      -- When `false`, it will use `WhichKeyIcon` instead
      colors = true,
      -- used by key format
      keys = {
        Up = " ",
        Down = " ",
        Left = " ",
        Right = " ",
        C = "󰘴 ",
        M = "󰘵 ",
        S = "󰘶 ",
        CR = "󰌑 ",
        Esc = "󱊷 ",
        ScrollWheelDown = "󱕐 ",
        ScrollWheelUp = "󱕑 ",
        NL = "󰌑 ",
        BS = "⌫",
        Space = "󱁐 ",
        Tab = "󰌒 ",
      },
    },
    show_help = true, -- show a help message in the command line for using WhichKey
    show_keys = true, -- show the currently pressed key and its label as a message in the command line
    -- Which-key automatically sets up triggers for your mappings.
    -- But you can disable this and setup the triggers yourself.
    -- Be aware, that triggers are not needed for visual and operator pending mode.
    triggers = {
      { "<auto>", mode = "nixsotc" },
    },
    disable = {
      -- disable WhichKey for certain buf types and file types.
      ft = {},
      bt = {},
    },
    debug = false, -- enable wk.log in the current directory
  },
  keys = {
    { "<leader>Q",  "<cmd>qa!<CR>",            desc = "Quit",         nowait = true, remap = false },
    { "<leader>bc", "<cmd>Bdelete!<CR>",       desc = "Close Buffer", nowait = true, remap = false },
    { "<leader>ee", "<cmd>NvimTreeToggle<cr>", desc = "Explorer",     nowait = true, remap = false },
    { "<leader>g",  group = "Git",             nowait = true,         remap = false },
    {
      "<leader>gR",
      "<cmd>lua require 'gitsigns'.reset_buffer()<cr>",
      desc = "Reset Buffer",
      nowait = true,
      remap = false,
    },
    { "<leader>gb", "<cmd>Telescope git_branches<cr>", desc = "Checkout branch", nowait = true, remap = false },
    { "<leader>gc", "<cmd>Telescope git_commits<cr>",  desc = "Checkout commit", nowait = true, remap = false },
    { "<leader>gg", "<cmd>lua _LAZYGIT_TOGGLE()<CR>",  desc = "Lazygit",         nowait = true, remap = false },
    { "<leader>h",  "<cmd>nohlsearch<CR>",             desc = "No Highlight",    nowait = true, remap = false },
    { "<leader>l",  group = "LSP",                     nowait = true,            remap = false },
    { "<leader>lI", "<cmd>LspInstallInfo<cr>",         desc = "Installer Info",  nowait = true, remap = false },
    {
      "<leader>lS",
      "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>",
      desc = "Workspace Symbols",
      nowait = true,
      remap = false,
    },
    {
      "<leader>la",
      "<cmd>lua vim.lsp.buf.code_action()<cr>",
      desc = "Code Action",
      nowait = true,
      remap = false,
    },
    {
      "<leader>ld",
      "<cmd>Telescope diagnostics bufnr=0<cr>",
      desc = "Document Diagnostics",
      nowait = true,
      remap = false,
    },
    {
      "<leader>lf",
      "<cmd>lua vim.lsp.buf.format{async=true}<cr>",
      desc = "Format",
      nowait = true,
      remap = false,
    },
    {
      "<leader>li",
      "<cmd>LspInfo<cr>",
      desc = "Info",
      nowait = true,
      remap = false,
    },
    {
      "<leader>lj",
      "<cmd>lua vim.diagnostic.goto_next()<CR>",
      desc = "Next Diagnostic",
      nowait = true,
      remap = false,
    },
    {
      "<leader>lk",
      "<cmd>lua vim.diagnostic.goto_prev()<cr>",
      desc = "Prev Diagnostic",
      nowait = true,
      remap = false,
    },
    {
      "<leader>ll",
      "<cmd>lua vim.lsp.codelens.run()<cr>",
      desc = "CodeLens Action",
      nowait = true,
      remap = false,
    },
    {
      "<leader>lq",
      "<cmd>lua vim.diagnostic.setloclist()<cr>",
      desc = "Quickfix",
      nowait = true,
      remap = false,
    },
    {
      "<leader>lr",
      "<cmd>lua vim.lsp.buf.rename()<cr>",
      desc = "Rename",
      nowait = true,
      remap = false,
    },
    {
      "<leader>ls",
      "<cmd>Telescope lsp_document_symbols<cr>",
      desc = "Document Symbols",
      nowait = true,
      remap = false,
    },
    {
      "<leader>lw",
      "<cmd>Telescope diagnostics<cr>",
      desc = "Workspace Diagnostics",
      nowait = true,
      remap = false,
    },
    { "<leader>s",  group = "Search",                      nowait = true,            remap = false },
    { "<leader>sC", "<cmd>Telescope commands<cr>",         desc = "Commands",        nowait = true, remap = false },
    { "<leader>sM", "<cmd>Telescope man_pages<cr>",        desc = "Man Pages",       nowait = true, remap = false },
    { "<leader>sR", "<cmd>Telescope registers<cr>",        desc = "Registers",       nowait = true, remap = false },
    { "<leader>sb", "<cmd>Telescope git_branches<cr>",     desc = "Checkout branch", nowait = true, remap = false },
    { "<leader>sc", "<cmd>Telescope colorscheme<cr>",      desc = "Colorscheme",     nowait = true, remap = false },
    { "<leader>sh", "<cmd>Telescope help_tags<cr>",        desc = "Find Help",       nowait = true, remap = false },
    { "<leader>sk", "<cmd>Telescope keymaps<cr>",          desc = "Keymaps",         nowait = true, remap = false },
    { "<leader>t",  group = "Terminal",                    nowait = true,            remap = false },
    { "<leader>tf", "<cmd>ToggleTerm direction=float<cr>", desc = "Float",           nowait = true, remap = false },
    {
      "<leader>th",
      "<cmd>ToggleTerm size=10 direction=horizontal<cr>",
      desc = "Horizontal",
      nowait = true,
      remap = false,
    },
    { "<leader>tn", "<cmd>lua _NODE_TOGGLE()<cr>",   desc = "Node",   nowait = true, remap = false },
    { "<leader>tp", "<cmd>lua _PYTHON_TOGGLE()<cr>", desc = "Python", nowait = true, remap = false },
    { "<leader>tt", "<cmd>lua _HTOP_TOGGLE()<cr>",   desc = "Htop",   nowait = true, remap = false },
    { "<leader>tu", "<cmd>lua _NCDU_TOGGLE()<cr>",   desc = "NCDU",   nowait = true, remap = false },
    {
      "<leader>tv",
      "<cmd>ToggleTerm size=80 direction=vertical<cr>",
      desc = "Vertical",
      nowait = true,
      remap = false,
    },
    { "<leader>w", "<cmd>w!<CR>", desc = "Save", nowait = true, remap = false },
  },
}
