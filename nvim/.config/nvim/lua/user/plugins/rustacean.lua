return {
  "mrcjkb/rustaceanvim",
  version = "^5", -- Recommended
  lazy = false,  -- This plugin is already lazy
  server = {
    default_settings = {
      ["rust-analyzer"] = {
        enable = false,
      },
    },
  },
}
