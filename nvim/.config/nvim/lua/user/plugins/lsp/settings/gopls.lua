local status_ok, util = pcall(require, "lspconfig/util")
if not status_ok then
  vim.notify("unable to load lspconfig/util")
  return
end

return {
  cmd = {"gopls", "serve"},
  filetypes = {"go", "gomod"},
  root_dir = util.root_pattern("go.work", "go.mod", ".git"),
  settings = {
    gopls = {
      analyses = {
        unusedparams = true,
      },
      staticcheck = true,
    },
  },
}
