return {
	"nvimtools/none-ls.nvim",
	dependencies = {
		"jayp0521/mason-null-ls.nvim",
	},
	config = function()
		local null_ls = require("null-ls")
		-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
		local formatting = null_ls.builtins.formatting
		-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
		local diagnostics = null_ls.builtins.diagnostics

		null_ls.setup({
			debug = true,
			sources = {
				formatting.prettier.with({ extra_args = { "--no-semi", "--single-quote", "--jsx-single-quote" } }),
				formatting.isort.with({ extra_args = { "--profile", "black" } }),
				formatting.black,
				formatting.stylua,
				formatting.goimports_reviser,
				diagnostics.pylint,
			},
		})
		require("mason-null-ls").setup({
			ensure_installed = { "stylua", "jq", "goimports_reviser" },
		})
	end,
}
