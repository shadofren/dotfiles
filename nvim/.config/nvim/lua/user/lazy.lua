-- [[ Install `lazy.nvim` plugin manager ]]
--    https://github.com/folke/lazy.nvim e
--    `:help lazy.nvim.txt` for more info
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.uv.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

-- Autocommand that reloads neovim whenever you save the lazy.lua file
vim.cmd([[
  augroup lazy_user_config
    autocmd!
    autocmd BufWritePost lazy.lua source <afile> | Lazy sync
  augroup end
]])

-- Autocommand that create new shortcuts
vim.cmd([[
  augroup shortcuts
    autocmd BufWritePost ~/.bm* !shortcuts
  augroup end
]])

-- Install your plugins here
require("lazy").setup({
	{ import = "user.plugins" },
	{ import = "user.plugins.lsp" },
}, {
	checker = {
		enabled = true,
		notify = false,
	},
	change_detection = {
		notify = false,
	},
})
