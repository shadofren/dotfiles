--[[ vim.api.nvim_create_autocmd("TextYankPost", { ]]
--[[   desc = "Highlight when yanking (copying) text", ]]
--[[   group = vim.api.nvim_create_augroup("highlight-yank", { clear = true }), ]]
--[[   callback = function() ]]
--[[     vim.highlight.on_yank({ higroup = "YankHighlight", timeout = 10000 }) ]]
--[[   end, ]]
--[[ }) ]]
--
vim.api.nvim_create_user_command("W", function()
  local file = vim.fn.expand("%") -- Get the current file path
  if file == "" then
    print("No file to write!")
    return
  end

  -- Get the current buffer's content
  local lines = vim.api.nvim_buf_get_lines(0, 0, -1, true)
  local content = table.concat(lines, "\n") .. "\n" -- Join lines into a single string
  local temp_file = vim.fn.tempname()
  vim.api.nvim_command("write! " .. temp_file)

  -- Use sudo to move the temporary file to the target location
  local cmd = string.format(
    "SUDO_ASKPASS=%s sudo -A cp --no-preserve all %s %s",
    vim.fn.shellescape("/home/yuu/.scripts/tools/wofipass"),
    vim.fn.shellescape(temp_file),
    vim.fn.shellescape(file)
  )

  local result = vim.fn.system(cmd, content)

  if vim.v.shell_error == 0 then
    vim.cmd("edit!")
    print("File written as root.")
  else
    print("Failed to write file as root." .. result)
  end
end, { nargs = 0 })

